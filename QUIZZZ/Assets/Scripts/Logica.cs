﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1. Quem criou a lógica?", 5);
        p1.AddResposta("Platão",false);
        p1.AddResposta("Aristóteles", true);
        p1.AddResposta("Sócrates", false);
        p1.AddResposta("Tales de Mileto", false);

        ClassPergunta p2 = new ClassPergunta("2. O que é lógica?", 3);
        p2.AddResposta("É o estudo dos conhecimentos matemáticos complexos.", false);
        p2.AddResposta("Conhecimento inato do ser humano.", false);
        p2.AddResposta("Parte da filosofia que estuda os mitos.", true);
        p2.AddResposta("É uma parte da filosofia que estuda o fundamento, a estrutura e as expressões humanas do conhecimento.", false);

        ClassPergunta p3 = new ClassPergunta("3. O que é um algoritmo?", 6);
        p3.AddResposta("É uma receita que mostra passo a passo os procedimentos necessários para a resolução de uma tarefa.", true);
        p3.AddResposta("São números matemáticos.", false);
        p3.AddResposta("São expressões matemáticas complexas.", false);
        p3.AddResposta("É uma receita que mostra o que fazer em um problema.", false);

        ClassPergunta p4 = new ClassPergunta("4. Qual dessas palavras não é um tipo de algoritmo?", 2);
        p4.AddResposta("Pseudocódigo.", false);
        p4.AddResposta("Fluxograma", false);
        p4.AddResposta("Descrição intuitiva.", true);
        p4.AddResposta("Diagrama de Chapin.", false);

        ClassPergunta p5 = new ClassPergunta("5. O que é uma variável", 5);
        p5.AddResposta("É um local na memória destinado a guardar um cálculo informado pelo usuário..", false);
        p5.AddResposta("É um objeto (uma posição, frequentemente localizada na memória) capaz de reter e representar um valor ou expressão.", true);
        p5.AddResposta("São onde os cálculos realizados pelos processadores são armazenados.", false);
        p5.AddResposta("É uma equação matemática que armazena os números irracionais.", false);

        ClassPergunta p6 = new ClassPergunta("6. Dentre as alternativas abaixo assinale a que não é considerada um dos tipos básicos de dados que a linguagem de programação irá manipular.", 5);
        p6.AddResposta("Dados alfanumérico.", false);
        p6.AddResposta("Dados Lógicos.", false);
        p6.AddResposta("Dados numéricos.", false);
        p6.AddResposta("Dados vitais.", true);

        ClassPergunta p7 = new ClassPergunta("7. Dentre as palavras abaixo qual não é considerada um tipo de operador em programação?", 5);
        p7.AddResposta("Lógicos.", false);
        p7.AddResposta("Aritméticos.", false);
        p7.AddResposta("Descrição", true);
        p7.AddResposta("Relacionais.", false);

        ClassPergunta p8 = new ClassPergunta("8. Os dados lógicos podem assumirem que tipo de valores?", 5);
        p8.AddResposta("3,15,103", false);
        p8.AddResposta("'A, B, C, D'", false);
        p8.AddResposta("Verdadeiro e falso", true);
        p8.AddResposta("2,14 3,14 -58", false);

        ClassPergunta p9 = new ClassPergunta("9. Qual é o símbolo usado no visualG para atribuição?", 5);
        p9.AddResposta("<", false);
        p9.AddResposta("=", false);
        p9.AddResposta("<=", false);
        p9.AddResposta("<-", true);

        ClassPergunta p10 = new ClassPergunta("10. Qual é o símbolo do operador relacional no visualG para - diferente?", 5);
        p10.AddResposta("<>", false);
        p10.AddResposta("=/-", false);
        p10.AddResposta("<=", false);
        p10.AddResposta(">=", true);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
